@extends('layouts.master')
@section('content')
<div class="row justify-content-center mt-5 pt-5" >
    <div class="container" style="background-color:rgb(70, 83, 83); color:lightgrey ; width: 910px; height:500px; overflow: scroll" >
            {{$img="",$nombre="",$p1=0,$p2=0,$p3=0,$tipo=""}}

        @foreach ($listareservas as $key => $reserva )
            <div class="row" style="background-color:darkviolet ">
                @foreach ($listasitios as $key1 => $sitio) 
                    @if ($sitio->id == $reserva->id_sitio)
                        <input type="hidden" value= "{{$img= $sitio->galeriaS}}" /> 
                        <input type="hidden" value= "{{$nombre = $sitio->nombre}}"/>
                        <input type="hidden" value= "{{$p1 = $sitio->precio}}"/>
                    @endif          
                @endforeach

                @foreach ($listatransportes as $key3 => $tra) 
                    @if ($tra->id == $reserva->id_transporte)
                        <input type="hidden" value= "{{$tipo = $tra->tipo}}">
                        <input type="hidden" value= "{{$p2 = $tra->precio}}">
                    @endif
                @endforeach

                @foreach ($listahospedajes as $key2 => $hosp) 
                    @if ($hosp->id == $reserva->id_hospedaje)
                        <input type="hidden" value= "{{$p3 = $hosp->precio}}"/>
                    @endif
                @endforeach
                <img src="{{ url($img)}}" style="height: 100px" />
                <h6>nombre: {{$nombre}}
                Tipo transporte: {{$tipo}}
                Precio: {{$p1+$p2+$p3}}
                fecha salida: {{$reserva->fecha_ini}}
                fecha llegada: {{$reserva->fecha_fin}}</h6>                                 
            </div>
        @endforeach
        <a type="button" class="btn btn-warning" href="{{url('/')}}">Volver</a>   
    </div>
</div>
@stop