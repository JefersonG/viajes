<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
  
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="{{url('/assets/bootstrap/css/bootstrap.min.css') }}">  
    <link rel="stylesheet" href="{{url('/assets/bootstrap/css/login.css') }}">
    <link rel="stylesheet" href="{{url('/assets/bootstrap/css/estilos.css') }}">


    <title>Viajes</title>
  </head>
  <body>
    @include('partials.navbarHome')
    <div class="container">
       @yield('content')
    </div>

    <script src="{{ url('/assets/bootstrap/js/bootstrap.min.js') }}"></script>
    <script src="{{ url('/assets/bootstrap/js/login.js') }}"></script>
    <script src="{{ url('/assets/bootstrap/js/popup.js') }}"></script>


    <script src="https://unpkg.com/ionicons@5.4.0/dist/ionicons.js"></script>
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-Piv4xVNRyMGpqkS2by6br4gNJ7DXjqk09RmUpJ8jgGtD7zP9yug3goQfGII0yAns" crossorigin="anonymous"></script>

  </body>
</html>
