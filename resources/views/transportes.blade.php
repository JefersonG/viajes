@extends('layouts.master')
@section('content')
    @if(Auth::user()->rol=="ADMIN")
        <div class="row justify-content-center mt-5 pt-5" >
            <div class="col-md-1"></div>
            
            <div class="col-md-10" style="background-color:rgb(70, 83, 83); color:lightgrey ">
                <p class="tittle" style="background-color:rgb(109, 111, 111)" align="center"> Aministracion de transportes </p>
                <button class="btn btn-success btn-lg btn-block" id="btn-abrir-popup">
                    <ion-icon name="add-circle-outline"></ion-icon>
                    Agregar un transporte
                </button>
            </div>
            
            <div class="col-md-1"></div>
        </div>


        <div class="container" style="background-color:rgb(70, 83, 83); color:lightgrey; width: 950px; height:500px; overflow: scroll" >
            <div class="row justify-content-center mt-5 pt-5">
                @foreach ($listatransportes as $key => $transporte )
                    <form method="POST" action="" id="miFormulario">
                        {{method_field('PUT')}}
                        {{ csrf_field()}}
                        <div class="col-md-1"></div>
                        <div class="col-md-10"  style="background-color:rgb(70, 83, 83); color:lightgrey">
                            <input type="hidden" name="id" id ="id" value="{{$transporte->id}}" />
                            <h5>Tipo: {{$transporte->tipo}}</h5>
                            <h5>Placas: {{$transporte->placas}}</h5>
                            <button type="Danger" class="btn btn-primary" style="padding:8px 100px;margin-top:25px;">
                                Eliminar
                            </button>
                        </div>
                        <div class="col-md-1"></div>
                    </form>
                @endforeach
            </div>
        </div>

        <div class="contenedor">    
            <div class= "overlay" id="overlay">
                <div class="popup" id="popup"> 
                    <a href="" id="btn-cerrar-popup" name="btn-cerrar-popup" class="btn-cerrar-popup">
                        <ion-icon name="close-outline"></ion-icon>
                    </a>
                    <h3> Digite los campos </h3>
                    <form action="" method="POST">
                        {{ csrf_field()}}
                        <div class="contenedor-inputs">
                            <input id ="tipo" name="tipo" type="text" placeholder="Tipo de transporte">
                            <input id ="placas" name="placas" type="text" placeholder="Placas del vehiculo">
                            <input id ="precio" name="precio" type="number" placeholder="Precio">
                            <input type="submit" class ="btn-submit" value="Crear Hospedaje">
                        </div>
                    </form>

                </div>
            </div>
        </div>
    @endif
    <script>
        (function() {
          var form = document.getElementById('miFormulario');
          form.addEventListener('submit', function(event) {
            // si es false entonces que no haga el submit
            if (!confirm('Realmente desea eliminar?')) {
              event.preventDefault();
            }
          }, false);
        })();
    </script>
@stop