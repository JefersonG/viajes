@extends('layouts.master')
@section('content')
    <div class="container">
        <div class="row justify-content-center mt-5 pt-5">
            @foreach ($listaSitios as $key => $sitio)
                <div  class="col-md-3 card" style=" margin: 10px; background-color:rgba(255, 255, 255, 0.816)" >                    
                    <a href="{{ url('descripcion/' . $sitio->id ) }}" class="btn btn-outline-info">
                        <img src="{{$sitio->galeriaS }}" class="card-img-top">
                        <input type="hidden" value="{{$sitio->id}}" />
                        <div class="card-body" align="center">
                            <h5>{{$sitio->nombre}}
                                <ion-icon name="arrow-forward-circle-outline"></ion-icon>
                            </h5>
                        </div>
                    </a>
                </div>    
            @endforeach
        </div>
    </div>
@stop