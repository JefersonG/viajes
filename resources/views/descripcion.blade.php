@extends('layouts.master')
@section('content')
<form method="POST" action="">
    {{ csrf_field()}}
    <div class="row justify-content-center mt-5 pt-5">
        <input type="hidden" id="id_usuario" name="id_usuario" value="{{Auth::user()->id}}" />
        <div class="col-sm-1"></div> 

        <div class="col-sm-5" style="background-color:rgb(70, 83, 83)">
            <img src="{{ url($sitio->galeriaS)}}" style="height: 200px; width: 200px;" />
            <h5> {{$sitio->nombre}}</h5>
            <h5> {{$sitio->precio}}</h5>
            <h5> {{$sitio->descripcion}}</h5>
            <h5> 
                Fecha partida:<input type="date" name ="fecha_ini" id="fecha_ini">
            </h5>
            <h5>
                Fecha llegada:<input type="date" name ="fecha_fin" id="fecha_fin">
            </h5>
            <input type="hidden" id="id_sitio" name="id_sitio" value="{{$sitio->id}}" />
        </div>

        <div class="col-sm-5" style="background-color:rgb(70, 83, 83)">
            <div class="container" >
                <div class="container" style="background-color:rgb(70, 83, 83); color:lightgrey ; height:200px; overflow: scroll"">
                    @foreach ($listahospedajes as $key => $hospedaje )
                        <div class="row justify-content-center mt-5 pt-5">
                            <div class="col-sm-4" align="center">           
                                {{-- TODO: Imagen de hospedaje --}}  
                                <img src="{{ url($hospedaje->galeriaS)}}" style="height: 100px" />
                            </div> 

                            <div class="col-sm-8" style="background-color:rgb(70, 83, 83); color:lightgrey">
                                <h6>Nombre: {{$hospedaje->nombre}}</h6>
                                <h6>Dirección: {{$hospedaje->direccion}}</h6>
                                <h6>Precio: {{$hospedaje->precio}}</h6>
                                <input type="checkbox" id="id_hospedaje" name="id_hospedaje" value="{{$hospedaje->id}}">
                            </div>
                        </div>
                    @endforeach   
                </div>
                <div class="container" style="background-color:rgb(70, 83, 83); color:lightgrey ; height:200px; overflow: scroll">
                    @foreach ($listatransportes as $key => $transporte )
                        <div class="row justify-content-center mt-5 pt-5">
                            <div class="col-sm-12"  style="background-color:rgb(70, 83, 83); color:lightgrey">
                                <h6>Tipo: {{$transporte->tipo}}</h6>
                                <h6>Placas: {{$transporte->placas}}</h6>
                                <h6>Precio: {{$transporte->precio}}</h6>
                                <input type="checkbox" id="id_transporte" name="id_transporte" value="{{$transporte->id}}">
                            </div>
                        </div>
                    @endforeach 
                </div> 
            </div>
        </div>
    
        <div class="col-sm-1"></div>
    </div>
    <div class="row justify-content-center mt-5 pt-5">
        @if(Auth::check() and Auth::user()->rol=="CLIENT")
            <button type="submit" class="btn btn-success">Reservar</button>
            
        @else
            @if (Auth::check() and Auth::user()->rol!="ADMIN")
                <a href="{{ url('login/' . $sitio->id ) }}" class="btn btn-success">Logearse</a>
                <a href="{{ url('registro/' . $sitio->id ) }}" class="btn btn-success">Registrarse</a>
                
            @endif
        @endif
        
    </div>
</form>
@stop