@extends('layouts.master')
@section('content')
    @if(Auth::user()->rol=="ADMIN")
        <div class="row justify-content-center mt-5 pt-5" >
            <div class="col-md-1"></div>
            
            <div class="col-md-10" style="background-color:rgb(70, 83, 83); color:lightgrey ">
                <p class="tittle" style="background-color:rgb(109, 111, 111)" align="center"> Aministracion de hospedajes </p>
                <button class="btn btn-success btn-lg btn-block" id="btn-abrir-popup">
                    <ion-icon name="add-circle-outline"></ion-icon>
                    Agregar un hospedaje
                </button>
            </div>
            
            <div class="col-md-1"></div>
        </div>


        <div class="container" style="background-color:rgb(70, 83, 83); color:lightgrey; width:950px; height:500px; overflow: scroll" >

            @foreach ($listahospedajes as $key => $hospedaje )
                <form method="POST" action="" id="miFormulario">
                    {{method_field('PUT')}}
                    {{ csrf_field()}}
                    <div class="row justify-content-center mt-5 pt-5">
                         <div class="col-sm-4" align="center">           
                            {{-- TODO: Imagen de hospedaje --}}  
                            <img src="{{ url($hospedaje->galeriaS)}}" style="height: 100px" />
                            <input type="hidden" name="id" id ="id" value="{{$hospedaje->id}}" />
                        </div> 

                        <div class="col-sm-8" style="background-color:rgb(70, 83, 83); color:lightgrey">
                            <h5>Nombre: {{$hospedaje->nombre}}</h5>
                            <h5>Dirección: {{$hospedaje->direccion}}</h5>
                            <button type="Danger" class="btn btn-primary" style="padding:8px 100px;margin-top:25px;">
                                Eliminar
                            </button>
                            
                        </div>
                        
                        <div class="col-md-1"></div>
                    </div>
                </form>
            @endforeach
        </div>

        <div class="contenedor">    
            <div class= "overlay" id="overlay">
                <div class="popup" id="popup"> 
                    <a href="" id="btn-cerrar-popup" name="btn-cerrar-popup" class="btn-cerrar-popup">
                        <ion-icon name="close-outline"></ion-icon>
                    </a>
                    <h3> Digite los campos </h3>
                    <form action="" method="POST">
                        {{ csrf_field()}}
                        <div class="contenedor-inputs">
                            <input id ="nombre" name="nombre" type="text" placeholder="Nombre">
                            <input id ="nHabitaciones" name="nHabitaciones" type="number" placeholder="Numero de habitaciones">
                            <input id ="galeriaS" name="galeriaS" type="text" placeholder="Ruta de imagenes">
                            <input id ="precio" name="precio" type="number" placeholder="Precio">
                            <input id ="telefono" name="telefono" type="number" placeholder="Teléfono">
                            <input id ="direccion" name="direccion" type="text" placeholder="Dirección">
                            <select id ="zona" name="zona">
                                <option value="zona1">Zona1</option>
                                <option value="zona2" selected>Zona2</option>
                                <option value="zona3">Zona3</option>
                            </select>
                            <input type="submit" class ="btn-submit" value="Crear Hospedaje">
                        </div>
                    </form>

                </div>
            </div>
        </div>
    @endif
    <script>
        (function() {
          var form = document.getElementById('miFormulario');
          form.addEventListener('submit', function(event) {
            // si es false entonces que no haga el submit
            if (!confirm('Realmente desea eliminar?')) {
              event.preventDefault();
            }
          }, false);
        })();
    </script>
@stop