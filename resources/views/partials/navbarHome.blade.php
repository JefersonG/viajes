<nav class="navbar navbar-expand-lg navbar-light bg-dark">
  <a class="navbar-brand" href="{{url('/')}}" style="color:#777"><ion-icon name="bus"></ion-icon> Viajes Romelio S.A</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo02" aria-controls="navbarTogglerDemo02" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarTogglerDemo02">
    <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
      <li class="nav-item  {{ Request::is('viajes')}}">
        @if(Auth::check() and Auth::user()->rol=="ADMIN") <a class="nav-link" style="color:lightgrey"><ion-icon name="construct-outline"></ion-icon>   </a>
        @else <a class="nav-link" href="{{url('/')}}" style="color:lightgrey"><ion-icon name="bus-outline"></ion-icon> Viajes</a>
        @endif
      </li>
      <!-- aqui comprobar que no este logeado -->
      @if(Auth::check())
        @if(Auth::user()->rol=="ADMIN")
          <li class="nav-item {{ Request::is('admSitio')}}">
            <a class="nav-link" href="{{url('/admSitio')}}" style="color:lightgrey"><ion-icon name="map-outline"></ion-icon> Sitios</a>
          </li>
          <li class="nav-item {{ Request::is('admHosp')}}">
            <a class="nav-link" href="{{url('/admHosp')}}" style="color:lightgrey"><ion-icon name="home-outline"></ion-icon> Hospedajes</a>
          </li>
          <li class="nav-item {{ Request::is('admTra')}}">
            <a class="nav-link" href="{{url('/admTra')}}" style="color:lightgrey"><ion-icon name="subway-outline"></ion-icon> Transportes</a>
          </li>
        
        @else
          <li class="nav-item {{ Request::is('reservas')}}">
            <a class="nav-link" href="{{url('/reservas')}}" style="color:lightgrey"><ion-icon name="newspaper-outline"></ion-icon>Mis Reservas</a>
          </li>
        @endif
        
        <li class="nav-item">
          <form action="{{ url('/logout') }}" method="POST" style="display:inline">
              {{ csrf_field() }}
              <button type="submit" class="btn btn-link nav-link" style="display:inline;cursor:pointer;color:lightgrey">
                  <ion-icon name="exit-outline"></ion-icon>
                  Cerrar sesión
              </button>
          </form>
        </li>
      @else
        <li class="nav-item {{ Request::is('login')}}">
          <a class="nav-link" href="{{url('/login')}}" style="color:lightgrey"><ion-icon name="enter-outline"></ion-icon> Ingresar</a>
        </li>
        <li class="nav-item {{ Request::is('registro')}}">
          <a class="nav-link" href="{{url('/registro')}}" style="color:lightgrey"><ion-icon name="person-add-outline"></ion-icon> Registrarme</a>
        </li>
      @endif
      
      <!-- aqui comprobar que este logeado -->
      </ul>
    @if(Auth::check() and Auth::user()->rol=="ADMIN")
    @else
      <form class="form-inline my-2 my-lg-0">
        <input class="form-control mr-sm-2" type="search" placeholder="Buscar un sitio">
        <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Buscar</button>
      </form>    
    @endif
    
  </div>
</nav>