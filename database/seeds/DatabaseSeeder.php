<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\App;
use App\Hospedaje;
use App\Reserva;
use App\Sitio;
use App\Transporte;
use App\User;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    
    

    private function seedUsers()
    {
        DB::table('users')->delete();
        $user = new User;
        $user->cc = "1087425177";
        $user->nombre = "Juanito Alcachofa";
        $user->celular = "3155555555";
        $user->email = "ilovempanada@gmail.com";
        $user->rol = "ADMIN";
        $user->password = bcrypt("123");
        $user->save();

        $userb = new User;
        $userb->cc = "108745231";
        $userb->nombre = "Luis Estupiñan";
        $userb->celular = "3154534432";
        $userb->email = "estradal@gmail.com";
        $userb->password = bcrypt("123");
        $userb->save();
    }

    private function seedSitio()
    {
        DB::table('sitios')->delete();
        $sit = new Sitio;
        $sit->nombre = "Laguna Verde";
        $sit->descripcion = "Soy del verde soy feliz";
        $sit->precio = 20;
        $sit->galeriaS = "https://http2.mlstatic.com/D_NQ_NP_684577-MCO32927542498_112019-O.jpg";
        $sit->zona = "zona1";
        $sit->save();

        $sit1 = new Sitio;
        $sit1->nombre = "Laguna Verde";
        $sit1->descripcion = "Soy del verde soy feliz";
        $sit1->precio = 20;
        $sit1->galeriaS = "https://http2.mlstatic.com/D_NQ_NP_684577-MCO32927542498_112019-O.jpg";
        $sit1->zona = "zona1";
        $sit1->save();
    }

    private function seedHospedaje()
    {
        DB::table('hospedajes')->delete();
        $hosp = new Hospedaje;
        $hosp->nombre = "Casa Grande";
        $hosp->nHabitaciones = 20;
        $hosp->galeriaS= "77-MCO32927542498_112019-O.jpg";
        $hosp->precio = 20;
        $hosp->telefono = "3133333333";
        $hosp->direccion = "CalleDebajaoDelPuente";
        $hosp->zona= "zona1";
        $hosp->save();
    }

    private function seedTransporte()
    {
        DB::table('transportes')->delete();
        $trans = new Transporte;
        $trans->tipo = "aerovan";
        $trans->placas = "145WAS";
        $trans->precio = 20;
        $trans->save();
    }

    public function run()
    {
        
        self::seedUsers();
        $this->command->info('Tabla Users inicilizada con datos');
        self::seedTransporte();
        $this->command->info('Tabla Transporte inicilizada con datos');
        self::seedHospedaje();
        $this->command->info('Tabla Hospedaje inicilizada con datos');
        self::seedSitio();
        $this->command->info('Tabla Sitio inicilizada con datos');
        // $this->call(UserSeeder::class);
    }
}
